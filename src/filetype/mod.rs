use std::fmt::{self, Debug, Display, Formatter};

pub mod wav;
pub mod zip;

#[derive(Debug, Clone)]
pub struct Buffer<'a> {
    buf: &'a [u8],
    offset: usize,
}

impl<'a, 'n> Buffer<'a> {
    pub fn new(buf: &'a [u8]) -> Self {
        Buffer { buf, offset: 0 }
    }

    fn next(&mut self) -> Option<u8> {
        if !self.buf.is_empty() {
            let b = self.buf[0];
            self.buf = &self.buf[1..];
            Some(b)
        } else {
            None
        }
    }

    pub fn read_uint8(&mut self, name: &'n str, style: ChunkStyle) -> Result<Chunk<'n>, ReadError> {
        let val = self.next().ok_or(ReadError::NotEnoughData)?;
        self.offset += 1;
        Ok(Chunk {
            offset: self.offset - 1,
            length: 1,
            data: ChunkData::UInt8(val),
            style,
            name,
        })
    }

    pub fn read_uint16_le(
        &mut self,
        name: &'n str,
        style: ChunkStyle,
    ) -> Result<Chunk<'n>, ReadError> {
        let val1 = self.next().ok_or(ReadError::NotEnoughData)? as u16;
        let val2 = self.next().ok_or(ReadError::NotEnoughData)? as u16;
        self.offset += 2;
        Ok(Chunk {
            offset: self.offset - 2,
            length: 2,
            data: ChunkData::UInt16((val2 << 8) + val1),
            style,
            name,
        })
    }

    pub fn read_uint16_be(
        &mut self,
        name: &'n str,
        style: ChunkStyle,
    ) -> Result<Chunk<'n>, ReadError> {
        let val1 = self.next().ok_or(ReadError::NotEnoughData)? as u16;
        let val2 = self.next().ok_or(ReadError::NotEnoughData)? as u16;
        self.offset += 2;
        Ok(Chunk {
            offset: self.offset - 2,
            length: 2,
            data: ChunkData::UInt16((val1 << 8) + val2),
            style,
            name,
        })
    }

    pub fn read_uint32_le(
        &mut self,
        name: &'n str,
        style: ChunkStyle,
    ) -> Result<Chunk<'n>, ReadError> {
        let val1 = self.next().ok_or(ReadError::NotEnoughData)? as u32;
        let val2 = self.next().ok_or(ReadError::NotEnoughData)? as u32;
        let val3 = self.next().ok_or(ReadError::NotEnoughData)? as u32;
        let val4 = self.next().ok_or(ReadError::NotEnoughData)? as u32;
        self.offset += 4;
        Ok(Chunk {
            offset: self.offset - 4,
            length: 4,
            data: ChunkData::UInt32((val4 << 24) + (val3 << 16) + (val2 << 8) + val1),
            style,
            name,
        })
    }

    pub fn read_uint32_be(
        &mut self,
        name: &'n str,
        style: ChunkStyle,
    ) -> Result<Chunk<'n>, ReadError> {
        let val1 = self.next().ok_or(ReadError::NotEnoughData)? as u32;
        let val2 = self.next().ok_or(ReadError::NotEnoughData)? as u32;
        let val3 = self.next().ok_or(ReadError::NotEnoughData)? as u32;
        let val4 = self.next().ok_or(ReadError::NotEnoughData)? as u32;
        self.offset += 4;
        Ok(Chunk {
            offset: self.offset - 4,
            length: 4,
            data: ChunkData::UInt32((val1 << 24) + (val2 << 16) + (val3 << 8) + val4),
            style,
            name,
        })
    }

    pub fn read_buffer(
        &mut self,
        name: &'n str,
        len: usize,
        style: ChunkStyle,
    ) -> Result<Chunk<'n>, ReadError> {
        let mut buf = vec![0; len];
        for i in 0..len {
            buf[i] = self.next().ok_or(ReadError::NotEnoughData)?;
        }
        self.offset += len;
        Ok(Chunk {
            offset: self.offset - len,
            length: len,
            data: ChunkData::Buffer(buf.into_boxed_slice()),
            style,
            name,
        })
    }
}

pub trait FileType {
    fn is_valid_header(&self, header: &[u8]) -> bool;
    fn parse<'a>(&self, buf: Buffer<'a>) -> Result<Vec<Chunk<'a>>, ReadError>;
}

#[derive(Debug)]
pub struct Chunk<'a> {
    offset: usize,
    length: usize,
    data: ChunkData,
    style: ChunkStyle,
    name: &'a str,
}

#[derive(Debug)]
pub enum ChunkData {
    UInt8(u8),
    UInt16(u16),
    UInt32(u32),
    Buffer(Box<[u8]>),
}

#[derive(Debug)]
pub struct ChunkStyle {
    display: DisplayStyle,
    importance: Importance,
}

impl ChunkStyle {
    pub fn new() -> Self {
        ChunkStyle {
            display: DisplayStyle::Auto,
            importance: Importance::Chunk,
        }
    }

    pub fn version_number() -> Self {
        Self::new().decimal()
    }

    pub fn length() -> Self {
        Self::new().hex_and_decimal()
    }

    pub fn offset() -> Self {
        Self::new().hex_and_decimal()
    }

    pub fn checksum() -> Self {
        Self::new().hex()
    }

    pub fn bit_flag() -> Self {
        Self::new().binary()
    }

    pub fn buffer() -> Self {
        Self::new().hex()
    }

    pub fn text_buffer() -> Self {
        Self::new().ascii()
    }

    pub fn value() -> Self {
        Self::new().hex_and_decimal()
    }

    pub fn section(self) -> Self {
        ChunkStyle {
            importance: Importance::Section,
            ..self
        }
    }

    pub fn chunk(self) -> Self {
        ChunkStyle {
            importance: Importance::Chunk,
            ..self
        }
    }

    pub fn part(self) -> Self {
        ChunkStyle {
            importance: Importance::Part,
            ..self
        }
    }

    pub fn binary(self) -> Self {
        ChunkStyle {
            display: DisplayStyle::Binary,
            ..self
        }
    }

    pub fn hex(self) -> Self {
        ChunkStyle {
            display: DisplayStyle::Hex,
            ..self
        }
    }

    pub fn decimal(self) -> Self {
        ChunkStyle {
            display: DisplayStyle::Decimal,
            ..self
        }
    }

    pub fn hex_and_decimal(self) -> Self {
        ChunkStyle {
            display: DisplayStyle::HexAndDecimal,
            ..self
        }
    }

    pub fn ascii(self) -> Self {
        ChunkStyle {
            display: DisplayStyle::Ascii,
            ..self
        }
    }

    pub fn auto(self) -> Self {
        ChunkStyle {
            display: DisplayStyle::Auto,
            ..self
        }
    }
}

#[derive(Debug, Clone, Copy)]
enum Importance {
    Section,
    Chunk,
    Part,
}

#[derive(Debug, Clone, Copy)]
enum DisplayStyle {
    Binary,
    Hex,
    Decimal,
    HexAndDecimal,
    Ascii,
    Auto,
}

#[derive(Debug)]
pub enum ReadError {
    NotEnoughData,
}

impl ChunkData {
    #[track_caller]
    pub fn u8(&self) -> u8 {
        match *self {
            ChunkData::UInt8(x) => x,
            _ => panic!("u8() called on non-u8 ChunkData"),
        }
    }

    #[track_caller]
    pub fn u16(&self) -> u16 {
        match *self {
            ChunkData::UInt16(x) => x,
            _ => panic!("u16() called on non-u16 ChunkData"),
        }
    }

    #[track_caller]
    pub fn u32(&self) -> u32 {
        match *self {
            ChunkData::UInt32(x) => x,
            _ => panic!("u32() called on non-u32 ChunkData"),
        }
    }

    #[track_caller]
    pub fn buf(&self) -> &[u8] {
        match self {
            ChunkData::Buffer(x) => x,
            _ => panic!("buf() called on non-buf ChunkData"),
        }
    }
}

impl Display for Chunk<'_> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(
            f,
            "{:#8x} (length {:5}): {:32} ",
            self.offset, self.length, self.name
        )?;

        // TODO implement importance using termios colors

        const MAXIMUM_BUFFER_DISPLAY_LENGTH: usize = 48;
        use ChunkData::*;
        use DisplayStyle::*;
        match (self.style.display, &self.data) {
            (Binary, UInt8(n)) => write!(f, "{0:#010b}", n),
            (Binary, UInt16(n)) => write!(f, "{0:#018b}", n),
            (Binary, UInt32(n)) => write!(f, "{0:#034b}", n),

            (Hex, UInt8(n)) => write!(f, "{:#04x}", n),
            (Hex, UInt16(n)) => write!(f, "{:#06x}", n),
            (Hex, UInt32(n)) => write!(f, "{:#010x}", n),
            (Hex, Buffer(b)) | (Auto, Buffer(b)) => {
                for n in b.iter().take(MAXIMUM_BUFFER_DISPLAY_LENGTH) {
                    write!(f, "{:02x}", n)?;
                }
                if b.len() > MAXIMUM_BUFFER_DISPLAY_LENGTH {
                    write!(f, "...")?;
                }
                Ok(())
            }

            (Decimal, UInt8(n)) => write!(f, "{}", n),
            (Decimal, UInt16(n)) => write!(f, "{}", n),
            (Decimal, UInt32(n)) => write!(f, "{}", n),

            (HexAndDecimal, UInt8(n)) | (Auto, UInt8(n)) => write!(f, "{0:#04x} {0}", n),
            (HexAndDecimal, UInt16(n)) | (Auto, UInt16(n)) => write!(f, "{0:#06x} {0}", n),
            (HexAndDecimal, UInt32(n)) | (Auto, UInt32(n)) => write!(f, "{0:#010x} {0}", n),

            (Ascii, UInt8(n)) => write!(f, "{}", *n as char),
            (Ascii, Buffer(b)) => {
                for n in b.iter().take(MAXIMUM_BUFFER_DISPLAY_LENGTH) {
                    if n >= &32 && n <= &126 {
                        write!(f, "{}", *n as char)?;
                    } else {
                        write!(f, ".")?;
                    }
                }
                if b.len() > MAXIMUM_BUFFER_DISPLAY_LENGTH {
                    write!(f, "...")?;
                }
                Ok(())
            }

            (style, data) => {
                write!(f, "(formatting error)")?;
                eprintln!("Unsupported style {:?} for data {:?}", style, data);
                Ok(())
            }
        }
    }
}
