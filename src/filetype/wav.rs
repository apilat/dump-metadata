use super::{Buffer, Chunk, ChunkStyle as Style, FileType, ReadError};

#[derive(Debug, Clone)]
pub struct Wav;

impl FileType for Wav {
    fn is_valid_header(&self, header: &[u8]) -> bool {
        header.len() >= 12
            && header[0..4] == [0x52, 0x49, 0x46, 0x46]
            && header[8..12] == [0x57, 0x41, 0x56, 0x45]
    }

    fn parse<'a>(&self, mut file: Buffer<'a>) -> Result<Vec<Chunk<'a>>, ReadError> {
        let mut chunks = Vec::new();

        chunks.push(file.read_buffer("riff_header", 4, Style::text_buffer().section())?);
        chunks.push(file.read_uint32_le("riff_chunk_length", Style::length())?);
        chunks.push(file.read_buffer("riff_type", 4, Style::text_buffer())?);

        loop {
            let mut chunk = match file.read_uint32_be("__tmp__", Style::new().hex().section()) {
                Ok(x) => x,
                Err(ReadError::NotEnoughData) => break,
            };
            let header = chunk.data.u32();
            chunk.name = match header {
                0x666d7420 => "fmt_chunk",
                0x66616374 => "fact_chunk",
                0x64617461 => "data_chunk",
                _ => "unknown_chunk",
            };
            chunks.push(chunk);

            let chunk = file.read_uint32_le("chunk_size", Style::length())?;
            let size = chunk.data.u32() as usize;
            chunks.push(chunk);

            match header {
                0x666d7420 => self.process_fmt_chunk(&mut file, &mut chunks)?,
                0x66616374 => {
                    chunks.push(file.read_buffer("extra_fact_bytes", size, Style::buffer())?)
                }
                0x64617461 => chunks.push(file.read_buffer("samples", size, Style::buffer())?),
                _ => chunks.push(file.read_buffer("unknown_chunk_data", size, Style::buffer())?),
            }
        }

        Ok(chunks)
    }
}

impl Wav {
    fn process_fmt_chunk<'a>(
        &self,
        file: &mut Buffer<'a>,
        chunks: &mut Vec<Chunk<'a>>,
    ) -> Result<(), ReadError> {
        chunks.push(file.read_uint16_le("compression_code", Style::value())?);
        chunks.push(file.read_uint16_le("channels", Style::value())?);
        chunks.push(file.read_uint32_le("sample_rate", Style::value())?);
        chunks.push(file.read_uint32_le("avg_bytes_per_second", Style::value())?);
        chunks.push(file.read_uint16_le("block_align", Style::value())?);
        chunks.push(file.read_uint16_le("bits_per_sample", Style::value())?);

        let chunk = file.read_uint16_le("extra_format_bytes_length", Style::value())?;
        let size = chunk.data.u16() as usize;
        chunks.push(chunk);
        chunks.push(file.read_buffer("extra_format_bytes", size, Style::buffer())?);

        Ok(())
    }
}
