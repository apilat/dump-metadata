use super::{Buffer, Chunk, ChunkStyle as Style, FileType, ReadError};

#[derive(Debug, Clone)]
pub struct Zip;

impl FileType for Zip {
    fn is_valid_header(&self, header: &[u8]) -> bool {
        header.len() >= 4 && header[0..4] == [0x50, 0x4b, 0x03, 0x04]
    }

    fn parse<'a>(&self, mut file: Buffer<'a>) -> Result<Vec<Chunk<'a>>, ReadError> {
        let mut chunks = Vec::new();

        loop {
            match file.read_uint32_be("__zip_tmp__", Style::new().section().hex()) {
                Ok(mut c) => {
                    let header = c.data.u32();
                    c.name = match header {
                        0x504b0304 => "local_file_header",
                        0x504b0708 => "data_descriptor",
                        0x504b0102 => "central_directory_file_header",
                        0x504b0506 => "eocd_header",
                        _ => "unknown_header",
                    };
                    chunks.push(c);
                    match header {
                        0x504b0304 => self.process_local_file_header(&mut file, &mut chunks)?,
                        0x504b0708 => self.process_data_descriptor(&mut file, &mut chunks)?,
                        0x504b0102 => {
                            self.process_central_directory_file_header(&mut file, &mut chunks)?
                        }
                        0x504b0506 => self.process_eocd_header(&mut file, &mut chunks)?,
                        _ => (),
                    };
                }
                Err(ReadError::NotEnoughData) => break,
            }
        }

        Ok(chunks)
    }
}

impl Zip {
    fn process_local_file_header<'a>(
        &self,
        file: &mut Buffer<'a>,
        chunks: &mut Vec<Chunk<'a>>,
    ) -> Result<(), ReadError> {
        chunks.push(file.read_uint16_le("minimum_version_to_extract", Style::version_number())?);
        chunks.push(file.read_uint16_le("bit_flag", Style::bit_flag())?);
        chunks.push(file.read_uint16_le("compression_method", Style::new())?);
        // TODO implement ms-dos modification time style
        chunks.push(file.read_uint16_le("last_modification_time", Style::new())?);
        chunks.push(file.read_uint16_le("last_modification_date", Style::new())?);
        chunks.push(file.read_uint32_le("crc32_uncompressed_data", Style::checksum())?);

        let chunk = file.read_uint32_le("compessed_size", Style::length())?;
        let compressed_size = chunk.data.u32();
        chunks.push(chunk);

        chunks.push(file.read_uint32_le("uncompessed_size", Style::length())?);

        let chunk = file.read_uint16_le("filename_length", Style::length())?;
        let filename_length = chunk.data.u16();
        chunks.push(chunk);
        let chunk = file.read_uint16_le("extra_field_length", Style::length())?;
        let extra_length = chunk.data.u16();
        chunks.push(chunk);

        chunks.push(file.read_buffer(
            "filename",
            filename_length as usize,
            Style::text_buffer(),
        )?);
        chunks.push(file.read_buffer("extra_field", extra_length as usize, Style::buffer())?);
        chunks.push(file.read_buffer("data", compressed_size as usize, Style::buffer())?);

        Ok(())
    }

    fn process_data_descriptor<'a>(
        &self,
        file: &mut Buffer<'a>,
        chunks: &mut Vec<Chunk<'a>>,
    ) -> Result<(), ReadError> {
        chunks.push(file.read_uint32_le("crc32_uncompressed_data", Style::length())?);
        chunks.push(file.read_uint32_le("compressed_size", Style::length())?);
        chunks.push(file.read_uint32_le("uncompressed_size", Style::length())?);
        Ok(())
    }

    fn process_central_directory_file_header<'a>(
        &self,
        file: &mut Buffer<'a>,
        chunks: &mut Vec<Chunk<'a>>,
    ) -> Result<(), ReadError> {
        chunks.push(file.read_uint16_le("version_made_by", Style::version_number())?);
        chunks.push(file.read_uint16_le("minimum_version_to_extract", Style::version_number())?);
        chunks.push(file.read_uint16_le("bit_flag", Style::bit_flag())?);
        chunks.push(file.read_uint16_le("compression_method", Style::new())?);
        chunks.push(file.read_uint16_le("last_modification_time", Style::new())?);
        chunks.push(file.read_uint16_le("last_modification_date", Style::new())?);
        chunks.push(file.read_uint32_le("crc32_uncompressed_data", Style::checksum())?);
        chunks.push(file.read_uint32_le("compessed_size", Style::length())?);
        chunks.push(file.read_uint32_le("uncompessed_size", Style::length())?);

        let chunk = file.read_uint16_le("filename_length", Style::length())?;
        let filename_length = chunk.data.u16();
        chunks.push(chunk);
        let chunk = file.read_uint16_le("extra_field_length", Style::length())?;
        let extra_length = chunk.data.u16();
        chunks.push(chunk);
        let chunk = file.read_uint16_le("file_comment_length", Style::length())?;
        let file_comment_length = chunk.data.u16();
        chunks.push(chunk);

        chunks.push(file.read_uint16_le("first_disk", Style::offset())?);
        chunks.push(file.read_uint16_le("internal_file_attributes", Style::new())?);
        chunks.push(file.read_uint32_le("external_file_attributes", Style::new())?);
        chunks.push(file.read_uint32_le("relative_offset_to_header", Style::length())?);

        chunks.push(file.read_buffer(
            "filename",
            filename_length as usize,
            Style::text_buffer(),
        )?);
        chunks.push(file.read_buffer("extra_field", extra_length as usize, Style::buffer())?);
        chunks.push(file.read_buffer(
            "file_comment",
            file_comment_length as usize,
            Style::text_buffer(),
        )?);

        Ok(())
    }

    fn process_eocd_header<'a>(
        &self,
        file: &mut Buffer<'a>,
        chunks: &mut Vec<Chunk<'a>>,
    ) -> Result<(), ReadError> {
        chunks.push(file.read_uint16_le("disk_number", Style::offset())?);
        chunks.push(file.read_uint16_le("central_directory_disk_number", Style::offset())?);
        chunks.push(file.read_uint16_le("this_disk_central_directories", Style::length())?);
        chunks.push(file.read_uint16_le("total_central_directories", Style::length())?);
        chunks.push(file.read_uint32_le("central_directory_size", Style::length())?);
        chunks.push(file.read_uint32_le("central_directory_offset", Style::offset())?);

        let chunk = file.read_uint16_le("comment_length", Style::length())?;
        let comment_length = chunk.data.u16();
        chunks.push(chunk);
        chunks.push(file.read_buffer("comment", comment_length as usize, Style::text_buffer())?);

        Ok(())
    }
}
