#![feature(min_specialization)]

use std::fs;
mod filetype;
use filetype::Buffer;
use filetype::{wav::Wav, zip::Zip, FileType};

fn main() {
    let args: Vec<String> = std::env::args().collect();
    let file_name = match args.get(1) {
        Some(x) => x,
        None => {
            eprintln!("Usage: dump-metadata <file>");
            return;
        }
    };

    let file = fs::read(file_name).expect("Couldn't read file");
    let header_len = usize::min(16, file.len());
    let header = &file[..header_len];

    let filetypes: [Box<dyn FileType>; 2] = [Box::new(Zip), Box::new(Wav)];
    if let Some(filetype) = filetypes.iter().find(|x| x.is_valid_header(header)) {
        for chunk in filetype.parse(Buffer::new(&file)).expect("Parsing failed") {
            println!("{}", chunk);
        }
    } else {
        eprintln!("Unrecognized file format");
    }
}
